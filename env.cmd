@echo off
cd /d %~dp0
set curdir=%cd%

@echo on
setx JAVA7_HOME "%curdir%\jdk1.7.0_80" -m
setx JAVA8_HOME "%curdir%\jdk1.8.0_65" -m
setx M2_HOME "%curdir%\apache-maven-3.3.3" -m
setx SCALA_HOME "%curdir%\scala-2.11.8" -m
setx SBT_HOME "%curdir%\sbt-0.13.11" -m

setx JAVA_HOME "%%JAVA7_HOME%%" -m
setx ClassPath ".;%%JAVA_HOME%%\lib\dt.jar;%%JAVA_HOME%%\lib\tools.jar" -m

setx Path "%%SystemRoot%%\system32;%%SystemRoot%%;%%SystemRoot%%\System32\Wbem;%%SYSTEMROOT%%\System32\WindowsPowerShell\v1.0\;%%JAVA_HOME%%\jre\bin;%%JAVA_HOME%%\bin;%%M2_HOME%%\bin;%%SCALA_HOME%%\bin;%%SBT_HOME%%\bin"

@pause